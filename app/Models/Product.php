<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['image', 'name', 'phone', 'address', 
    'product_price', 'advance_tk', 'delivery_fee', 'due_tk', 
    'description'];

    public function pictures()
    {
        return $this->morphMany(Image::class, 'imagable');
    }
}
