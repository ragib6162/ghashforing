<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Image;
use App\Http\Requests\ProductRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;
use PDF;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
               $products = Product::all();

        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $products = Product::orderBy('created_at', 'desc')->paginate(20);
        return view('welcome', compact('products', 'sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data = $request->all();
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // $data['due_tk'] = ($data['product_price'] + $data['delivery_fee']) - $data['advance_tk'];

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        // dd();
        try {      
              $data = $request->all();

              $data['due_tk'] = ($data['product_price'] + $data['delivery_fee']) - $data['advance_tk'];

            //   dd($data);

              if ($request->hasFile('image')){
                  $image = $request->file('image');
                //   dd( $image);
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $file_name = $timestamp .'-'. '.' . $image->getClientOriginalExtension();

                $image->move(public_path('uploads/products/'), $file_name);


                $data['image'] = $file_name;
            }

           
                //    dd($data);
               $product = Product::create($data);

                if($request->hasFile('picture')){
                    $sl = 0;
                    foreach ($request->picture as $pic){
                    
                        $image = $pic;
                        //   dd( $image);
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $file_name = $timestamp .'-'.$sl. '.' . $image->getClientOriginalExtension();
                        $image->move(public_path('uploads/products/'), $file_name);
    
                        $product->pictures()->create([
    
                            'picture' => $file_name,
                        ]);
                        $sl++;
                    }
                }
            
            
            
                        return redirect()->route('products.index')->withStatus('Created Successfully !');
                    }catch (QueryException $e){
                       return redirect()->back()->withInput()->withErrors($e->getMessage());
                    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // dd($product);

        // $pdf = PDF::loadView('backend.products.show', compact('product'));
        // return $pdf->download('product.pdf');
        return view('backend.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // $product = Product::findOrFail($product);
        // dd($product);

        return view('backend.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

               try{
                //    dd($product->image);
                            $data = $request->all();
                            $data['due_tk'] = ($data['product_price'] + $data['delivery_fee']) - $data['advance_tk'];

                            // dd($data);

                            if ($request->hasFile('image')){

                                $this->unlink($product->image);

                                $image = $request->file('image');
                              //   dd( $image);
                              $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                              $file_name = $timestamp .'-'. '.' . $image->getClientOriginalExtension();
              
                              $image->move(public_path('uploads/products/'), $file_name);
              
              
                              $data['image'] = $file_name;
                          }

                            // if ($request->hasFile('image')) {
                            //     $this->unlink($product->image);

                            //     $data['image'] = $this->uploadImage($request->image, $request->title);
                            // }
                            // dd($data);

                            $product->update($data);

                            if($request->hasFile('picture') && count($request->picture) > 0){

                               
                                // dd($product->pictures);
                                foreach($product->pictures as $pics){
                                    
                                //$pic variable store the JSON formate data.
                                
                                $data = json_decode($pics);
                                // dd($pics);
                                    foreach($data as $value){
                                         
                                    $pathToUpload = public_path().'/uploads/products/';

                                    if ($value != '' && file_exists($pathToUpload. $value)) {
                                        // dd("hi");
                            
                                        @unlink($pathToUpload. $value);
                                    }

                                    }
                                    
                                }

                                $product->pictures()->delete();
                                $sl = 0;
                                foreach ($request->picture as $pic){
                                
                                    $image = $pic;
                                    //   dd( $image);
                                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                                    $file_name = $timestamp .'-'.$sl. '.' . $image->getClientOriginalExtension();
                                    $image->move(public_path('uploads/products/'), $file_name);
                
                                    // $product->pictures()->update([
                
                                    //     'picture' => $file_name,
                                    // ]);
                                    $image = new Image;
                                    $image->picture = $file_name;

                                    $product->pictures()->save($image);

                                    $sl++;
                                }
                            }


                            return redirect()->route('products.index')->withStatus('Updated Successfully !');
                        }catch (QueryException $e){
                            return redirect()->back()->withInput()->withErrors($e->getMessage());
                        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    private function unlink($file)
    {
        // dd($file);
        $pathToUpload = public_path().'/uploads/products/';
        // dd($pathToUpload);
        if ($file != '' && file_exists($pathToUpload. $file)) {
            // dd("hi");

            @unlink($pathToUpload. $file);
        }
    }

    // private function uploadImage($file)
    // {
    //    dd($file);
    //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
    //     // dd($timestamp);
    //     $file_name = $timestamp .'-'. '.' . $file->getClientOriginalExtension();
    //     $pathToUpload = storage_path().'/app/public/products/';
    //     // dd($pathToUpload);
    //     // dd($file);
    //     Image::make($file)->resize(80,100)->save($pathToUpload.$file_name);
    //             dd("something");
    //     return $file_name;
    // }


}
