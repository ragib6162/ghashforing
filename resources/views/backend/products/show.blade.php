<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container-fluid">

        <!-- DataTales Example -->
                    <div class="card shadow  mb-4">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col-md-6 text-primary"><b>Product Information</b></div>
                                <div class="col-md-6 text-right">
                                    <a href="{{ route('products.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                                </div>
                            </div>
                        </div>

                 <div class="card-body">

                                        
                                        <div class="d-inline-flex">
                                            <div class="p-1">
                                                <b ><h4 class="text-white " style="background-color: rgb(28, 1, 78); opacity:1; text-align:center"> Product Image : </h4></b>
                                                @if(file_exists(public_path('uploads/products/').$product->image ) && (!is_null($product->image)))
                                                <img src="{{ asset('uploads/products/'.$product->image) }}" height="400">
                                        
                                            @endif
                                                </div>
            
            
                                                <div class="p-1">
                                                    <b><h4 class="text-white" style="background-color: rgb(28, 1, 78); opacity:1; text-align:center"> Product Colors : </h4></b>
                                                    @foreach($product->pictures as $img)
                                                        @if(file_exists(public_path().'/uploads/products/'.$img->picture ) && (!is_null($img->picture)))
                                                            <img src="{{ asset('uploads/products/'.$img->picture) }}" height="200">
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div style="margin: 0px 20px 0px 0px;">
                                                    <table class="table table-striped">
                                                    <tbody>
                                                        <tr class="bg-success text-white">
                                                            <th>Order ID</th>
                                                            <th>{{ $product->id }}</th>
                                                        </tr>
                                                    <tr class="table-info">
                                                        <th>Cutomer Name</th>
                                                        <th>{{ $product->name }}</th>
                                                    </tr>
                                                    <tr class="table-primary">
                                                        <th>Phone</th>
                                                        <th>{{ $product->phone }}</th>
                                                    </tr>
                                                    <tr class="table-info">
                                                        <th>Product Price</th>
                                                        <th>{{ $product->product_price }}</th>
                                                    </tr>
                                                    <tr class="table-primary">
                                                        <th>Advance TK</th>
                                                        <th>{{ $product->advance_tk }}</th>
                                                    </tr>
                                                    <tr class="table-info">
                                                        <th>Delivery Fee</th>
                                                        <th>{{ $product->delivery_fee }}</th>
                                                    </tr>
                                                    <tr class="bg-danger text-white">
                                                        <th>Due TK</th>
                                                        <th>{{ $product->due_tk }}</th>
                                                    </tr>
                                                    <tr class="table-info">
                                                        <th>Address</th>
                                                        <th>{{ $product->address }}</th>
                                                    </tr>
                                                    <tr class="table-primary">
                                                        <th>Description</th>
                                                        <th>{{ $product->description }}</th>
                                                    </tr>
                                                    
            
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div  class="float-right text-success" style="margin: 90px 0px 0px 0px; " >
                                                <spna>                                           
                                                    <center><img src="{{asset('ghasforing.jpg')}}"  height="200" ></center> 
                                                </spna>
                                                <center><b><p>ঘাসফড়িং-Ghashforing</p></b></center> 
                                                {{-- <span style="color: red; alignment: center">Phone: +8801535450517</span> --}}
                                                <b><p style="color: #429207; text-align: center">Phone: +8801535450517</p></b>
                                                
    
    
                                            </div>
                                            
                                        </div>
                                    

                    
                        </div>
                    </div>

                </div>

</body>
</html>
