
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-3">


            <div class="form-row">
                <div class="form-group col-md-6 ">
                    {{Form::label('image','Product:Image', ['class' => 'text-primary'])}}<br>
                    {{Form::file('image', null, [
                          'class'=> $errors->has('image') ?  'form-control  is-invalid': 'form-control ',

                          'id'=>'image',
                    ])}}
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="form-group col-md-6 ">
                    {{ Form::label('picture', 'Multiple Product Image') }}<br>
                    {!! Form::file('picture[]',
                    ['multiple'=>true,'class'=>'']) !!}
                    @error('picture')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-4 ">
            {{--        <label for="title">Title</label>--}}
                    {{ Form::label('name', 'Customer: Name', ['class' => 'text-primary']) }}
            
            {{--        <input name="title" type="text" class="form-control" value="--}}
            {{--            @if(old('title') == '')--}}
            {{--                {{  isset($category) ? $category->title : null }}--}}
            {{--            @else--}}
            {{--             {{ old('title') }}--}}
            {{--            @endif--}}
            {{--            " placeholder="Title">--}}
                    {{ Form::text('name', null, [
                        'class' => $errors->has('name') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                        'placeholder' => 'Enter the product name',
                        'id' => 'name'
                    ]) }}
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group col-md-4 ">
                            {{ Form::label('phone', 'Phone: Number', ['class' => 'text-primary']) }}
                            {{ Form::text('phone', null, [
                                'class' => $errors->has('phone') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                                'placeholder' => 'Enter the phone number',
                                'id' => 'phone'
                            ]) }}
                            @error('phone')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
        


            </div>

            <div class="form-row">

                <div class="form-group col-md-4 ">
                    {{ Form::label('product_price', 'Product: Price', ['class' => 'text-primary']) }}
                    {{ Form::number('product_price', null, [
                        'class' => $errors->has('product_price') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                        'id' => 'product_price'
                    ]) }}
                    @error('product_price')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group col-md-4 ">
                    {{ Form::label('advance_tk', 'Advance TK', ['class' => 'text-primary']) }}
                    {{ Form::number('advance_tk', null, [
                        'class' => $errors->has('advance_tk') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                        'id' => 'advance_tk'
                    ]) }}
                    @error('advance_tk')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group col-md-4 ">
                    {{ Form::label('delivery_fee', 'Delivery Fee', ['class' => 'text-primary']) }}
                    {{ Form::number('delivery_fee', null, [
                        'class' => $errors->has('delivery_fee') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                        'id' => 'delivery_fee'
                    ]) }}
                    @error('delivery_fee')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-6 ">

                            {{ Form::label('address', 'Address', ['class' => 'text-primary']) }}
                            {{ Form::textarea('address', null, [
                                'class' => $errors->has('address') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                                'id' => 'address',
                                'rows' => '2',
                            ]) }}

                                @error('address')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                        </div>

                        <div class="form-group col-md-6 ">

                            
                            {{ Form::label('description', 'Description', ['class' => 'text-primary']) }}
                            {{ Form::textarea('description', null, [
                                'class' => $errors->has('description') ?  'form-control form-control-user is-invalid': 'form-control form-control-user',
                                'id' => 'description',
                                'rows' => '2',
                            ]) }}

                                @error('description')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror

                            </div>
                                
                        
            </div>
            
            {{Form::button('Submit form',[
                'class'=>'btn btn-primary',
                'type'=>'submit',
                   ])}}

        </div>
    </div>
</div>

