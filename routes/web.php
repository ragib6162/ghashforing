<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/',[App\Http\Controllers\ProductController::class, 'index'])->name('products.index');
// Route::get('/', 'ProductController@index')->name('products.index');
Route::get('/products/create',[App\Http\Controllers\ProductController::class, 'create'])->name('products.create');
Route::post('/products/store',[App\Http\Controllers\ProductController::class, 'store'])->name('products.store');
// Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');
Route::get('/products/{product}',[App\Http\Controllers\ProductController::class, 'show'])->name('products.show');

// Route::get('/categories/{category}/edit', 'CategoryController@edit')->name('categories.edit');
Route::get('/products/{product}/edit',[App\Http\Controllers\ProductController::class, 'edit'])->name('products.edit');
// Route::put('/categories/{category}', 'CategoryController@update')->name('categories.update');
Route::put('/products/{product}',[App\Http\Controllers\ProductController::class, 'update'])->name('products.update');
